package beroepsproduct2020;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.*;

/**
 *
 * De opdracht: Maak een ziekenhuis opname applicatie waarbij de gebruiker meerdere symptomen kan kiezen uit een lijst van (minimaal 10) symptomen
 * afkomstig van informatie in de database. Elk symptoom heeft een risk waarde LOW, MEDIUM of HIGH.
 * Na het selecteren krijgt de gebruiker een response dat hij/zij moet worden opgenomen, indien tenminste 1 van de geselecteerde symptomen HIGH is.
 * De responses voor eventuele combinaties van LOW en/of MEDIUM mogen zelf bepaald worden.
 * Symptomen mogen zelf bedacht worden.
 *
 * Eisen: 
 * - Verbinding met de database 
 * - De applicatie moet oneindig door kunnen gaan 
 * - Gebruik comments om toe te lichten - Geef duidelijke instructies 
 * - Geef duidelijke instructies aan de gebruiker
 * - Vermeld je naam en andere relevante gegevens in de comments
 *
 * Bonus punten: 
 * - De gebruiker nieuwe symptomen toevoegen aan de database
 * - De gebruiker kan symptomen aanpassen in de database
 *
 * Inleveren: 
 * - De werkende java code 
 * - Database export (data en structuur)
 * - Inleveren tot uiterlijk 12 juni 2020 om 23:59:59 SRT
 *
 */
public class Beroepsproduct2020 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

//         create a new connection from MySQLJDBCUtil
        try (Connection conn = getConnection()) {

            // print out a message
            System.out.println(String.format("Connected to database %s "
                    + "successfully.", conn.getCatalog()));
            /* uncomment dit pas wanneer je een database verbinding hebt gemaakt */
            // run query
            String sql = "SELECT * "
                    + "FROM symptoom";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql); 
            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getString("id") + "\t"
                        + rs.getString("symptoom") + "\t"
                        + rs.getString("risicofactor"));

            }
            /**/
            // close the connection
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    /**
     * Get database connection
     *
     * @return a Connection object
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException {
        Connection conn = null;

        // assign db parameters
        String url = "jdbc:mysql://localhost:3306/";
        String user = "sis_user";
        String password = "?H@n3V$+MF%XXvH";
        String databasename = "sis";

        // create a connection to the database
        conn = DriverManager.getConnection(url + databasename, user, password);

        return conn;
        
        // Dit is een test door EM
        // Dit is een test door AE
        // Dit is een test door MM
    }
}
